<?php

namespace Drupal\Tests\backfill_formatter\Kernel;

use Drupal\backfill_formatter\BackFillTerms;
use Drupal\Core\Database\Query\AlterableInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;
use Drupal\Tests\taxonomy_entity_index\Kernel\TaxonomyEntityIndexKernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Defines a class for testing back-fill by term service.
 *
 * @group backfill_formatter
 */
class BackFillByTermTest extends TaxonomyEntityIndexKernelTestBase {

  use ContentTypeCreationTrait;
  use UserCreationTrait;
  use NodeCreationTrait;
  use BackFillByTermsTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $entityTypes = [
    'entity_test',
    'node',
  ];

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'text',
    'options',
    'backfill_formatter',
    'filter',
    'node',
    'options',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->installConfig(['filter']);
    $this->installSchema('node', 'node_access');
  }

  /**
   * {@inheritdoc}
   */
  protected function createBundles($entity_type_id) {
    if ($entity_type_id === 'entity_test') {
      entity_test_create_bundle('termz1');
      entity_test_create_bundle('termz2');
      entity_test_create_bundle('termz3');
      entity_test_create_bundle('termz4');
      $this->setupReferenceField('termz1', 'related',
        ['termz1', 'termz2', 'termz3']);
      $this->setupReferenceField('termz2', 'related',
        ['termz1', 'termz2', 'termz3']);
      $this->setupReferenceField('termz3', 'related', ['termz1', 'termz2']);
      return ['termz1', 'termz2', 'termz3', 'termz4'];
    }
    if ($entity_type_id === 'node') {
      $this->installConfig(['node']);
      $this->createContentType(['type' => 'page']);
      $this->setupReferenceField('page', 'related', ['page'], 'node');
      return ['page'];
    }
    return [];
  }

  /**
   * Tests back-filling.
   */
  public function testBackFill() {
    $vocab1 = $this->createVocabulary();
    $vocab2 = $this->createVocabulary();
    $vocab3 = $this->createVocabulary();

    $term1 = $this->createTerm($vocab1);
    $term2 = $this->createTerm($vocab1);
    $term3 = $this->createTerm($vocab1);
    $term4 = $this->createTerm($vocab2);
    $term5 = $this->createTerm($vocab2);
    $term6 = $this->createTerm($vocab2);
    $term7 = $this->createTerm($vocab3);

    $entity1 = $this->createTestEntity([$term1]);
    $entity2 = $this->createTestEntity([$term1]);
    $entity3 = $this->createTestEntity([$term1]);
    $this->createTestEntity([$term2]);

    // Test simple back-fill - common-terms.
    $this->assertBackfill($entity1, [$entity3, $entity2], 2);

    // Test where not enough entities with common terms.
    $this->assertBackfill($entity1, [$entity3, $entity2], 3);

    // Test vocabulary filtering.
    // This doesn't yield $entity4 because we don't give vocab2 a shot.
    $entity5 = $this->createTestEntity([$term1, $term4]);
    $this->assertBackfill($entity5, [$entity3, $entity2, $entity1], 3, [$vocab1->id() => 1]);

    // Test number of terms in common.
    // Entity 7 should come first because there are two terms in common.
    // Entity 5 should match next because it is the newest.
    $entity6 = $this->createTestEntity([$term1, $term2]);
    $entity7 = $this->createTestEntity([$term1, $term2]);
    $this->assertBackfill($entity6, [$entity7, $entity5], 2);

    // Test where entity doesn't have the field.
    $entity8 = $this->createTestEntity([$term1], 'termz4');
    $this->assertBackfill($entity8, [], 2);

    // Test where entity has enough values already, backfilling for less than or
    // equal to the existing values should not impact results, nor should vocab
    // filter.
    $entity9 = $this->createTestEntity([$term1], 'termz1', [
      'related' => [$entity7, $entity1, $entity8],
    ]);
    $this->assertBackfill($entity9, [$entity7, $entity1, $entity8], 0);
    $this->assertBackfill($entity9, [$entity7, $entity1, $entity8], 3);
    $this->assertBackfill($entity9, [$entity7, $entity1, $entity8], 2);
    $this->assertBackfill($entity9, [$entity7, $entity1, $entity8], 2, ['not a vocab' => 1]);
    // Simulate access to entity8 is denied, so should back-fill, despite their
    // being 3 items in the field.
    $this->assertBackfill($entity9, [$entity7, $entity1, $entity6], 3, [],
      BackFillTerms::MODE_MATCH_ANY, [$entity8->id()]);
    $this->assertBackfill($entity9,
      [$entity7, $entity1, $entity8, $entity6, $entity5], 5);

    // Test where the field doesn't support a given bundle.
    $entity10 = $this->createTestEntity([$term3], 'termz3');
    // Entity 11.
    $this->createTestEntity([$term3], 'termz3');
    // Entity 12.
    $this->createTestEntity([$term3], 'termz3');
    $this->assertBackfill($entity10, [], 3);
    $entity13 = $this->createTestEntity([$term3], 'termz2');
    $this->assertBackfill($entity10, [$entity13], 3);

    // Test where there are multiple matching terms, but vocabulary weights.
    $entity14 = $this->createTestEntity([$term5]);
    $entity15 = $this->createTestEntity([$term1, $term2, $term3]);
    $entity16 = $this->createTestEntity([$term1, $term2, $term3, $term5]);
    // This only matches 14 because of the filter, despite more common terms
    // with entity 15.
    $this->assertBackfill($entity16, [$entity14], 2, [$vocab2->id() => 1]);
    // This matches 14 and 15 because of the common terms but 14 should come
    // first, because of vocabulary weights, even though there are more common
    // terms with 15.
    $this->assertBackfill($entity16, [$entity14, $entity15], 2, [
      $vocab2->id() => 1,
      $vocab1->id() => 2,
    ]);
    // The same should occur if the weights aren't pre-sorted.
    $this->assertBackfill($entity16, [$entity14, $entity15], 2, [
      $vocab1->id() => 2,
      $vocab2->id() => 1,
    ]);

    // Match all tests - 16 will not match 14 because 14 has no matching terms
    // in vocab 2.
    $this->assertBackfill($entity16, [], 2, [
      $vocab1->id() => 2,
      $vocab2->id() => 2,
    ], BackFillTerms::MODE_MATCH_ALL);

    $this->assertBackfill($entity16, [$entity14, $entity15], 2, [
      $vocab1->id() => 2,
      $vocab2->id() => 1,
    ], BackFillTerms::MODE_MATCH_ALL_WITH_FALLBACK);

    $entity17 = $this->createTestEntity([$term1, $term2, $term3, $term5]);
    $entity18 = $this->createTestEntity([$term1, $term2, $term5]);
    // This wil skew the results because it matches on vocab 2.
    $entity19 = $this->createTestEntity([$term5]);

    // Entity 17 has more terms in common and will come first.
    $this->assertBackfill($entity16, [$entity17, $entity18], 2, [
      $vocab1->id() => 2,
      $vocab2->id() => 1,
    ], BackFillTerms::MODE_MATCH_ALL);
    $entity19->delete();

    // Entity 14 matches in vocab2, so is the first fallback.
    $this->assertBackfill($entity16, [$entity17, $entity18, $entity14], 3, [
      $vocab1->id() => 2,
      $vocab2->id() => 1,
    ], BackFillTerms::MODE_MATCH_ALL_WITH_FALLBACK);

    // Test no matching entities.
    $entity20 = $this->createTestEntity([$term1]);
    $backfill_terms = \Drupal::service('backfill_formatter.backfill_terms');
    $this->assertEmpty($backfill_terms->findMatchingEntitiesByTerms($entity20, 'node', ['page'], 1, [$vocab1->id() => 1], BackFillTerms::MODE_MATCH_ALL));
    $this->assertEmpty($backfill_terms->findMatchingEntitiesByTerms($entity20, 'node', ['page'], 1, [$vocab1->id() => 1], BackFillTerms::MODE_MATCH_ANY));
    $this->assertEmpty($backfill_terms->findMatchingEntitiesByTerms($entity20, 'node', ['page'], 1, [$vocab1->id() => 1], BackFillTerms::MODE_MATCH_ALL_WITH_FALLBACK));

    // Test no matching terms in vocabulary provided and no matching entities.
    $entity21 = $this->createTestEntity([$term1]);
    $a_node = $this->createNode([
      'type' => 'page',
      'termz' => [$term1],
      'status' => 1,
    ]);
    $this->assertEmpty($backfill_terms->findMatchingEntitiesByTerms($entity21, 'node', ['page'], 1, [$vocab1->id() => 1, $vocab3->id() => 2], BackFillTerms::MODE_MATCH_ALL));
    $this->assertEmpty($backfill_terms->findMatchingEntitiesByTerms($entity21, 'node', ['page'], 1, [$vocab3->id() => 1, $vocab1->id() => 2], BackFillTerms::MODE_MATCH_ALL));
    $this->assertEquals($this->mapEntityIds([$a_node]), $this->mapEntityIds($backfill_terms->findMatchingEntitiesByTerms($entity21, 'node', ['page'], 1, [$vocab1->id() => 1, $vocab3->id() => 2], BackFillTerms::MODE_MATCH_ANY)));
    $this->assertEquals($this->mapEntityIds([$a_node]), $this->mapEntityIds($backfill_terms->findMatchingEntitiesByTerms($entity21, 'node', ['page'], 1, [$vocab1->id() => 1, $vocab3->id() => 2], BackFillTerms::MODE_MATCH_ALL_WITH_FALLBACK)));
    $this->assertEmpty($this->mapEntityIds($backfill_terms->findMatchingEntitiesByTerms($entity21, 'node', ['page'], 1, [$vocab1->id() => 1, $vocab3->id() => 2], BackFillTerms::MODE_MATCH_ALL_WITH_FALLBACK, [], function (AlterableInterface $query) use ($a_node) {
      $query->condition('entity_id', $a_node->id(), '<>');
    })));
    $a_node->delete();

    // Test no terms in entity, and no values in field.
    $entity22 = $this->createTestEntity([]);
    $this->assertBackfill($entity22, [], 5);

    // Test user-access.
    $this->setUpCurrentUser([], ['access content']);
    $published_node1 = $this->createNode([
      'type' => 'page',
      'termz' => [$term1],
      'status' => 1,
    ]);
    $published_node2 = $this->createNode([
      'type' => 'page',
      'termz' => [$term1],
      'status' => 1,
    ]);
    $unpublished_node = $this->createNode([
      'type' => 'page',
      'termz' => [$term1],
      'status' => 0,
    ]);
    // Unpublished node should not be seen here because it is not published.
    $this->assertBackfill($published_node1, [$published_node2], 2);

    $admin = $this->setUpCurrentUser([], [
      'access content',
      'bypass node access',
      'administer nodes',
    ]);
    $this->setCurrentUser($admin);
    // But should be seen for admin.
    $this->assertBackfill($published_node1, [$unpublished_node, $published_node2], 2);
  }

  /**
   * Asserts backfilling logic.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity to backfill field for.
   * @param array $expected
   *   Expected entities.
   * @param int $backfill_count
   *   Backfill count.
   * @param array $vocabulary_weights
   *   Vocabulary weights.
   * @param string $backfill_mode
   *   Backfill mode.
   * @param array $access_denied
   *   Simulate access denied for a referenced entity.
   */
  protected function assertBackfill(ContentEntityInterface $entity, array $expected, int $backfill_count, array $vocabulary_weights = [], string $backfill_mode = BackFillTerms::MODE_MATCH_ANY, array $access_denied = []) {
    $field_definition = $entity->getFieldDefinition('related') ?: FieldConfig::load('entity_test.termz1.related');
    $existing_values = $entity->hasField('related') ? \Drupal::entityTypeManager()
      ->getStorage($field_definition->getSetting('target_type'))
      ->loadMultiple(array_column($entity->get('related')->getValue(),
        'target_id')) : [];
    $this->assertEquals($this->mapEntityIds($expected),
      $this->mapEntityIds(\Drupal::service('backfill_formatter.backfill_terms')
        ->backfillEntity($entity, array_filter($existing_values, function (ContentEntityInterface $entity) use ($access_denied) {
          // Filter out access denied items.
          return !in_array($entity->id(), $access_denied, TRUE);
        }),
          $field_definition,
          $backfill_count, $vocabulary_weights, $backfill_mode)));
  }

  /**
   * Map entity IDs from entities.
   *
   * @param \Drupal\Core\Entity\EntityInterface[] $entities
   *   IDs to map.
   */
  protected function mapEntityIds(array $entities) {
    return array_values(array_map(function (EntityInterface $entity) {
      return sprintf('%s:%s', $entity->getEntityTypeId(), $entity->id());
    }, $entities));
  }

  /**
   * Creates a test entity.
   *
   * @param array $terms
   *   Terms for the entity.
   * @param string $bundle
   *   Bundle.
   * @param array $values
   *   Field values.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Created entity.
   */
  protected function createTestEntity(array $terms = [], $bundle = 'termz1', array $values = []) {
    $entity = EntityTest::create($values + [
      'type' => $bundle,
      'termz' => $terms,
    ]);
    $entity->save();
    return $entity;
  }

}
