<?php

namespace Drupal\Tests\backfill_formatter\Kernel;

use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;
use Drupal\Tests\taxonomy_entity_index\Kernel\TaxonomyEntityIndexKernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Defines a class for testing access cache on formatter.
 */
class FormatterAccessCacheTest extends TaxonomyEntityIndexKernelTestBase {

  use ContentTypeCreationTrait;
  use UserCreationTrait;
  use NodeCreationTrait;
  use BackFillByTermsTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $entityTypes = [
    'node',
  ];

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'text',
    'options',
    'backfill_formatter',
    'backfill_formatter_access_test',
    'filter',
    'node',
    'options',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->installConfig(['filter', 'system']);
    $this->installSchema('node', 'node_access');
  }

  /**
   * {@inheritdoc}
   */
  protected function createBundles($entity_type_id) {
    if ($entity_type_id === 'node') {
      $this->installConfig(['node']);
      $this->createContentType(['type' => 'page']);
      $this->createContentType(['type' => 'nope']);
      $this->setupReferenceField('page', 'related', ['page', 'nope'], 'node');
      return ['page', 'nope'];
    }
    return [];
  }

  /**
   * Tests that access is respected and applied and that cache data is accurate.
   */
  public function testFormatterAccessCache() {
    $this->setupCurrentUser([], ['bypass node access', 'access content']);

    $vocab1 = $this->createVocabulary();
    $term1 = $this->createTerm($vocab1);

    $ok_node1 = $this->createNode([
      'type' => 'page',
      'termz' => [$term1],
      'status' => 1,
    ]);
    $ok_node2 = $this->createNode([
      'type' => 'page',
      'termz' => [$term1],
      'status' => 1,
    ]);
    // @see backfill_formatter_access_test_node_access().
    $private_node = $this->createNode([
      'type' => 'nope',
      'termz' => [$term1],
      'status' => 1,
    ]);
    $ok_node3 = $this->createNode([
      'type' => 'page',
      'termz' => [$term1],
      'status' => 1,
    ]);

    /** @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display */
    $display = \Drupal::service('entity_display.repository')->getViewDisplay('node', 'page');
    $display->setComponent('related', [
      'type' => 'backfill_formatter_terms',
      'settings' => [
        'quantity' => 3,
        'view_mode' => 'teaser',
        'vocabularies' => [
          $vocab1->id() => [
            'enabled' => TRUE,
            'weight' => -50,
          ],
        ],
      ],
    ]);
    $display->save();

    $view_builder = \Drupal::entityTypeManager()->getViewBuilder('node');
    $build = $view_builder->view($ok_node1);
    $output = (string) \Drupal::service('renderer')->renderPlain($build);
    $this->assertContains($ok_node2->label(), $output);
    $this->assertContains($ok_node3->label(), $output);
    $this->assertContains($private_node->label(), $output);

    $this->setCurrentUser($this->createUser(['access content']));
    $build = $view_builder->view($ok_node1);
    $output = (string) \Drupal::service('renderer')->renderPlain($build);
    $this->assertContains($ok_node2->label(), $output);
    $this->assertContains($ok_node3->label(), $output);
    $this->assertNotContains($private_node->label(), $output);

    $ok_node4 = $this->createNode([
      'type' => 'page',
      'termz' => [$term1],
      'status' => 1,
    ]);
    $build = $view_builder->view($ok_node1);
    $output = (string) \Drupal::service('renderer')->renderPlain($build);
    $this->assertContains($ok_node4->label(), $output);
    $this->assertContains($ok_node3->label(), $output);
    $this->assertNotContains($private_node->label(), $output);

    // Assert that having the item in the field does not ignore access.
    $ok_node1->related = [$private_node, $ok_node4];
    $ok_node1->save();
    $build = $view_builder->view($ok_node1);
    $output = (string) \Drupal::service('renderer')->renderPlain($build);
    $this->assertContains($ok_node3->label(), $output);
    $this->assertContains($ok_node4->label(), $output);
    $this->assertNotContains($private_node->label(), $output);
  }

}
