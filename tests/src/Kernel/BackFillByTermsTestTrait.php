<?php

namespace Drupal\Tests\backfill_formatter\Kernel;

use Drupal\Component\Utility\Unicode;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Defines a trait for back-fill formatter test functionality.
 */
trait BackFillByTermsTestTrait {

  /**
   * Creates a new entity reference field for the given bundle.
   *
   * @param string $bundle
   *   Bundle of field.
   * @param string $field_name
   *   Field name.
   * @param array $allowed_bundles
   *   Allowed bundles.
   * @param string $entity_type_id
   *   Entity type.
   */
  protected function setupReferenceField(
    string $bundle,
    string $field_name,
    array $allowed_bundles,
    string $entity_type_id = 'entity_test'
  ) {
    if (!FieldStorageConfig::load("$entity_type_id.$field_name")) {
      $storage = FieldStorageConfig::create([
        'entity_type' => $entity_type_id,
        'field_name' => $field_name,
        'id' => "$entity_type_id.$field_name",
        'type' => 'entity_reference',
        'settings' => [
          'target_type' => $entity_type_id,
        ],
      ]);
      $storage->save();
    }

    if (!FieldConfig::load("$entity_type_id.$bundle.$field_name")) {
      /** @var \Drupal\field\FieldConfigInterface $config */
      $config = FieldConfig::create([
        'field_name' => $field_name,
        'entity_type' => $entity_type_id,
        'bundle' => $bundle,
        'id' => "$entity_type_id.$bundle.$field_name",
        'label' => Unicode::ucfirst($field_name),
      ]);
      $config->setSetting('handler_settings', [
        'target_bundles' => $allowed_bundles,
      ]);
      $config->save();
    }
  }

}
