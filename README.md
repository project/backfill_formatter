
Contents of this file
---------------------

 * About this module
 * Use cases
 * Demo
 * Installation
 * Known Issues/Limitations


About this Module
-----------------

Provides a field formatter for entity reference fields that allows back-filling values when a field
is not populated by finding content with matching terms.

Items with the most matching terms are shown first.

The formatter allows you to configure:

 * the view-mode used to render the related items;
 * the quantity of items to display (up to the field maximum); and
 * which vocabularies to give precedence to when selecting matching terms


Use Cases
---------

- Add a 'related articles' field that allows curation but with a fallback if not populated


Demo
----

> Evaluate this project online using [simplytest.me](https://simplytest.me/project/backfill_formatter).


Installation
------------

1. `composer require "drupal/backfill_formatter"`

2. Enable the module using drush or in 'Extend'. (/admin/modules)

3. Find a content-type or entity you want to use the formatter on and visit its 'Manage display' page

4. Select the 'Back-fill by terms' formatter and configure as required.


Known Issues/Limitations
------------------------

- Layout builder won't show empty fields that use this formatter, apply the patch from [this core bug](https://www.drupal.org/project/drupal/issues/3067982)


Author/Maintainer
-----------------

- [larowlan](https://drupal.org/u/larowlan)
