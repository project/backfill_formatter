<?php

namespace Drupal\backfill_formatter\Plugin\Field\FieldFormatter;

use Drupal\backfill_formatter\BackFillTerms;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceEntityFormatter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\VocabularyInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class for a formatter for back-filling entities using common terms.
 *
 * @FieldFormatter(
 *   id = "backfill_formatter_terms",
 *   label = @Translation("Back-fill by terms"),
 *   description = @Translation("Display the referenced entities rendered by entity_view() with back-filling using common-terms."),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class BackFillByTermsFormatter extends EntityReferenceEntityFormatter {

  /**
   * Access cache.
   *
   * @var \Drupal\Core\Cache\CacheableMetadata[]
   */
  private $accessCache = [];

  /**
   * Back fill terms service.
   *
   * @var \Drupal\backfill_formatter\BackFillTerms
   */
  private $backFillTerms;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  private $logger;

  /**
   * Sets back fill terms service.
   *
   * @param \Drupal\backfill_formatter\BackFillTerms $backFillTerms
   *   Service.
   *
   * @return $this
   */
  private function setBackFillTerms(BackFillTerms $backFillTerms) : BackFillByTermsFormatter {
    $this->backFillTerms = $backFillTerms;
    return $this;
  }

  /**
   * Sets logger.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   Logger.
   *
   * @return $this
   */
  private function setLogger(LoggerInterface $logger) : BackFillByTermsFormatter {
    $this->logger = $logger;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    /** @var \Drupal\backfill_formatter\Plugin\Field\FieldFormatter\BackFillByTermsFormatter $instance */
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    return $instance->setBackFillTerms($container->get('backfill_formatter.backfill_terms'))
      ->setLogger($container->get('logger.factory')->get('backfill_formatter'));
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return parent::defaultSettings() + [
      'quantity' => 0,
      'vocabularies' => [],
      'mode' => BackFillTerms::MODE_MATCH_ANY,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $build = parent::settingsForm($form, $form_state);
    $max = $this->fieldDefinition->getFieldStorageDefinition()->getCardinality();
    $build['quantity'] = [
      '#type' => 'number',
      '#min' => 1,
      '#title' => $this->t('Quantity'),
      '#default_value' => $this->getSetting('quantity'),
    ];
    if ($max !== FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED) {
      $build['quantity']['#max'] = $max;
      if (!$this->getSetting('quantity')) {
        $build['quantity']['#default_value'] = $max;
      }
    }
    $vocabularies = array_map(function (VocabularyInterface $vocabulary) {
      return $vocabulary->label();
    }, $this->entityTypeManager->getStorage('taxonomy_vocabulary')->loadMultiple());
    $weights = $this->getSetting('vocabularies');
    $build['vocabularies_caption'] = [
      '#type' => 'item',
      '#markup' => $this->t('Enabled vocabularies'),
    ];
    $build['vocabularies'] = [
      '#type' => 'table',
    ];
    $build['vocabularies']['#tabledrag'][] = [
      'action' => 'order',
      'relationship' => 'sibling',
      'group' => 'backfill-formatter-vocabulary-weight',
    ];
    foreach ($vocabularies as $vid => $label) {
      $build['vocabularies'][$vid] = [
        '#weight' => $weights[$vid]['weight'] ?? 50,
        'enabled' => [
          '#title_display' => 'invisible',
          '#type' => 'checkbox',
          '#title' => $this->t('Enable for vocabulary %label', ['%label' => $label]),
          '#default_value' => !empty($weights[$vid]['enabled']),
        ],
        'label' => [
          '#plain_text' => $label,
        ],
        'weight' => [
          '#type' => 'weight',
          '#title' => $this->t('Weight for vocabulary %label', ['%label' => $label]),
          '#title_display' => 'invisible',
          '#delta' => 50,
          '#default_value' => $weights[$vid]['weight'] ?? 50,
          '#attributes' => [
            'class' => ['backfill-formatter-vocabulary-weight'],
          ],
        ],
        '#attributes' => [
          'class' => ['draggable', 'js-form-wrapper'],
        ],
      ];
    }
    uasort($build['vocabularies'], ['Drupal\Component\Utility\SortArray', 'sortByWeightProperty']);

    $build['mode'] = [
      '#type' => 'select',
      '#default_value' => $this->getSetting('mode'),
      '#title' => $this->t('Match mode'),
      '#options' => [
        BackFillTerms::MODE_MATCH_ANY => $this->t('Any'),
        BackFillTerms::MODE_MATCH_ALL => $this->t('All'),
        BackFillTerms::MODE_MATCH_ALL_WITH_FALLBACK => $this->t('All with fallback'),
      ],
      '#description' => $this->t('To match on any terms in vocabulary order, select <em>Any</em>. If the matched items must have at least one common term in each enabled vocabulary, select <em>All</em>. To try and select items with a common term in each enabled vocabulary but fallback to any common terms in vocabulary order to achieve the configured quantity, use <em>Any with fallback</em>. With the <em>All</em> and <em>All with fallback</em> modes, each additional enabled vocabulary increases the number of queries which could have a performance impact.'),
    ];
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $summary[] = $this->t('Back-filling to @quantity', [
      '@quantity' => $this->getSetting('quantity'),
    ]);
    $vocabularies = array_map(function (VocabularyInterface $vocabulary) {
      return $vocabulary->label();
    }, $this->entityTypeManager->getStorage('taxonomy_vocabulary')->loadMultiple());
    $weights = $this->getSetting('vocabularies');
    $items = [];
    foreach ($vocabularies as $vid => $label) {
      if (empty($weights[$vid]['enabled'])) {
        continue;
      }
      $items[] = $this->t('@name (@weight)', [
        '@name' => $label,
        '@weight' => $weights[$vid]['weight'] ?? 0,
      ]);
    }
    if ($items) {
      $summary[] = $this->t('Vocabularies: @vocabularies', [
        '@vocabularies' => implode(', ', $items),
      ]);
      return $summary;
    }
    $summary[] = $this->t('All vocabularies');
    return $summary;
  }

  /**
   * Allows extension by custom callback.
   *
   * @param \Drupal\Core\Field\EntityReferenceFieldItemListInterface $items
   *   Items for the formatter.
   *
   * @return callable|null
   *   Query callback.
   *
   * @see \Drupal\backfill_formatter\BackFillTerms::backfillEntity
   */
  protected function getQueryCallback(EntityReferenceFieldItemListInterface $items): ?callable {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntitiesToView(EntityReferenceFieldItemListInterface $items, $langcode) {
    $entities = parent::getEntitiesToView($items, $langcode);
    $cache = new CacheableMetadata();
    $vocabularies = array_map(function (array $item) {
      return $item['weight'];
    }, array_filter($this->getSetting('vocabularies'), function (array $item) {
      return !empty($item['enabled']);
    }));
    $back_filled = $this->backFillTerms->backfillEntity($items->getEntity(), $entities, $this->fieldDefinition, $this->getSetting('quantity'), $vocabularies, $this->getSetting('mode'), $this->getQueryCallback($items));
    $allowed = [];
    foreach ($back_filled as $entity) {
      $access = $this->checkAccess($entity);
      $cache->merge(CacheableMetadata::createFromObject($access));
      if ($access->isAllowed()) {
        $allowed[] = $entity;
      }
    }
    $access_cache_id = $this->generateAccessCacheId($items, $langcode);
    $this->accessCache[$access_cache_id] = $cache;
    if (count($allowed) < count($back_filled)) {
      $this->logger->debug(sprintf('One or more entities that were selected by a back-fill query were denied access via an access rule that was not encapsulated in a query, consider implementing a custom BackFillQuery plugin for the %s entity type to include these rules.', $this->getFieldSetting('target_type')));
    }
    // This already contains the original parent method entities.
    return $allowed;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $access_cache_id = $this->generateAccessCacheId($items, $langcode);
    $build = parent::viewElements($items, $langcode);
    if (isset($this->accessCache[$access_cache_id])) {
      $this->accessCache[$access_cache_id]->applyTo($build);
    }
    $cache = new CacheableMetadata();
    $cache->addCacheTags($this->entityTypeManager->getDefinition($this->fieldDefinition->getSetting('target_type'))->getListCacheTags());
    $cache->applyTo($build);
    return $build;
  }

  /**
   * Generates an access cache ID.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   Items.
   * @param string $langcode
   *   Langcode.
   *
   * @return string
   *   Cache ID.
   */
  private function generateAccessCacheId(FieldItemListInterface $items, string $langcode) {
    $view_mode = $this->getSetting('view_mode');
    return hash('sha256', serialize($items->getValue())) . $langcode . $view_mode;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    $target_type_id = $field_definition->getSetting('target_type');
    return parent::isApplicable($field_definition) && \Drupal::entityTypeManager()->getDefinition($target_type_id)->entityClassImplements(ContentEntityInterface::class);
  }

}
