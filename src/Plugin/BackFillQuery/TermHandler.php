<?php

namespace Drupal\backfill_formatter\Plugin\BackFillQuery;

/**
 * Defines a class for term query handler.
 *
 * @BackFillQuery(
 *   id = "default:taxonomy_term",
 *   label = @Translation("Term"),
 * )
 */
class TermHandler extends PermissionStatusHandler {

  /**
   * {@inheritdoc}
   */
  protected function getPermission(): string {
    return 'administer taxonomy';
  }

}
