<?php

namespace Drupal\backfill_formatter\Plugin\BackFillQuery;

/**
 * Defines a class for user query handler.
 *
 * @BackFillQuery(
 *   id = "default:user",
 *   label = @Translation("User"),
 * )
 */
class UserHandler extends PermissionStatusHandler {

  /**
   * {@inheritdoc}
   */
  protected function getPermission(): string {
    return 'administer users';
  }

}
