<?php

namespace Drupal\backfill_formatter\Plugin\BackFillQuery;

use Drupal\Core\Database\Query\AlterableInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines a class for a query handler with permission-based a status filter.
 */
abstract class PermissionStatusHandler extends DefaultHandler {

  /**
   * Gets the admin permission that bypasses the status check.
   *
   * @return string
   *   Permission ID.
   */
  abstract protected function getPermission() : string;

  /**
   * Gets status key.
   *
   * @return string
   *   Status key.
   */
  protected function getStatusKey() : string {
    return 'status';
  }

  /**
   * {@inheritdoc}
   */
  public function alterQuery(
    EntityTypeManagerInterface $entity_type_manager,
    AlterableInterface $query,
    AccountInterface $account,
    string $langcode
  ) {
    parent::alterQuery($entity_type_manager, $query, $account, $langcode);
    $target_entity_type_id = $this->pluginDefinition['entity_type_id'];
    $storage = $entity_type_manager->getStorage($target_entity_type_id);
    if ($storage instanceof SqlContentEntityStorage && !$account->hasPermission($this->getPermission())) {
      $data_table = $storage->getDataTable();
      $entity_type_definition = $entity_type_manager->getDefinition($target_entity_type_id);
      $id_field = $entity_type_definition->getKey('id');
      $join_alias = $query->join($data_table, $data_table, sprintf('%s.%s = tei.entity_id', $data_table, $id_field));
      $query->condition(sprintf('%s.%s', $join_alias, 'langcode'), $langcode);
      $query->condition(sprintf('%s.%s', $join_alias, $this->getStatusKey()), 1);
    }
  }

}
