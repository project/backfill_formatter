<?php

namespace Drupal\backfill_formatter\Plugin\BackFillQuery;

/**
 * Defines a class for comment query handler.
 *
 * @BackFillQuery(
 *   id = "default:comment",
 *   label = @Translation("Comment"),
 * )
 */
class CommentHandler extends PermissionStatusHandler {

  /**
   * {@inheritdoc}
   */
  protected function getPermission(): string {
    return 'administer comments';
  }

}
