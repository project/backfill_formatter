<?php

namespace Drupal\backfill_formatter\Plugin\BackFillQuery;

/**
 * Defines a class for node query handler.
 *
 * @BackFillQuery(
 *   id = "default:node",
 *   label = @Translation("Node"),
 * )
 */
class NodeHandler extends PermissionStatusHandler {

  /**
   * {@inheritdoc}
   */
  protected function getPermission(): string {
    return 'bypass node access';
  }

}
