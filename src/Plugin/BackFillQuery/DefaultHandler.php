<?php

namespace Drupal\backfill_formatter\Plugin\BackFillQuery;

use Drupal\backfill_formatter\BackFillQueryPluginBase;
use Drupal\Core\Database\Query\AlterableInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines a class for a default back-fill query handler.
 *
 * @BackFillQuery(
 *   id = "default",
 *   label = @Translation("Default"),
 *   deriver = "Drupal\backfill_formatter\BackFillQueryEntityTypeDeriver"
 * )
 */
class DefaultHandler extends BackFillQueryPluginBase {

  /**
   * {@inheritdoc}
   */
  public function alterQuery(
    EntityTypeManagerInterface $entity_type_manager,
    AlterableInterface $query,
    AccountInterface $account,
    string $langcode
  ) {
    $target_entity_type_id = $this->pluginDefinition['entity_type_id'];
    $storage = $entity_type_manager->getStorage($target_entity_type_id);
    if ($storage instanceof SqlContentEntityStorage) {
      // Add code for node_access grants integration.
      $base_table = $storage->getBaseTable();
      $query->addTag($target_entity_type_id . '_access');
      $entity_type_definition = $entity_type_manager->getDefinition($target_entity_type_id);
      $id_field = $entity_type_definition->getKey('id');
      $join_alias = $query->join($base_table, $base_table, sprintf('%s.%s = tei.entity_id', $base_table, $id_field));
      $query->addMetaData('base_table', $join_alias);
    }
  }

}
