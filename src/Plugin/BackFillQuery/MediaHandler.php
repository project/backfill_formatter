<?php

namespace Drupal\backfill_formatter\Plugin\BackFillQuery;

/**
 * Defines a class for a media query handler.
 *
 * @BackFillQuery(
 *   id = "default:media",
 *   label = @Translation("Media"),
 * )
 */
class MediaHandler extends PermissionStatusHandler {

  /**
   * {@inheritdoc}
   */
  protected function getPermission(): string {
    return 'administer media';
  }

}
