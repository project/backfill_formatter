<?php

namespace Drupal\backfill_formatter;

use Drupal\Core\Plugin\PluginBase;

/**
 * Defines a class for a base plugin class.
 */
abstract class BackFillQueryPluginBase extends PluginBase implements BackFillQueryPluginInterface {

}
