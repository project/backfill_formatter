<?php

namespace Drupal\backfill_formatter;

use Drupal\backfill_formatter\Annotation\BackFillQuery;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Defines a class for a plugin manager for back-fill query handlers.
 */
class BackFillQueryPluginManager extends DefaultPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/BackFillQuery', $namespaces, $module_handler, BackFillQueryPluginInterface::class, BackFillQuery::class);
    $this->setCacheBackend($cache_backend, 'backfill_formatter_query_plugins');
    $this->alterInfo('backfill_formatter_query');
  }

}
