<?php

namespace Drupal\backfill_formatter;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class for deriving.
 */
class BackFillQueryEntityTypeDeriver extends DeriverBase implements ContainerDeriverInterface {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * Base plugin ID.
   *
   * @var string
   */
  private $basePluginId;

  /**
   * Constructs a new BackFillQueryEntityTypeDeriver.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Manager.
   * @param string $base_plugin_id
   *   Base ID.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, string $base_plugin_id) {
    $this->entityTypeManager = $entityTypeManager;
    $this->basePluginId = $base_plugin_id;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    $base_plugin_id
  ) {
    return new static($container->get('entity_type.manager'), $base_plugin_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {
      $this->derivatives[$entity_type_id] = $base_plugin_definition;
      $this->derivatives[$entity_type_id]['entity_type_id'] = $entity_type_id;
      $this->derivatives[$entity_type_id]['label'] = t('@entity_type selection', ['@entity_type' => $entity_type->getLabel()]);
      $this->derivatives[$entity_type_id]['base_plugin_label'] = (string) $base_plugin_definition['label'];
    }

    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}
