<?php

namespace Drupal\backfill_formatter\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a class for an annotation for back-fill query plugins.
 *
 * @Annotation
 */
class BackFillQuery extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The title of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

}
