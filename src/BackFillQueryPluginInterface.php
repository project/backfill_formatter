<?php

namespace Drupal\backfill_formatter;

use Drupal\Core\Database\Query\AlterableInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines an interface for back-fill query plugins.
 */
interface BackFillQueryPluginInterface {

  /**
   * Alter the query for this back fill query.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   * @param \Drupal\Core\Database\Query\AlterableInterface $query
   *   Query to alter.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Current user.
   * @param string $langcode
   *   Langcode.
   */
  public function alterQuery(EntityTypeManagerInterface $entity_type_manager, AlterableInterface $query, AccountInterface $account, string $langcode);

}
