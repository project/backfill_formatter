<?php

namespace Drupal\backfill_formatter;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines a class for backfilling entity-reference values using common terms.
 */
class BackFillTerms {

  const MODE_MATCH_ALL = 'all';
  const MODE_MATCH_ANY = 'any';
  const MODE_MATCH_ALL_WITH_FALLBACK = 'all_fallback';

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * Database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  private $database;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $currentUser;

  /**
   * Query plugin manager.
   *
   * @var \Drupal\backfill_formatter\BackFillQueryPluginManager
   */
  private $backFillQueryPluginManager;

  /**
   * Constructs a new BackfillTerms.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Database\Connection $database
   *   Database.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   Current user.
   * @param \Drupal\backfill_formatter\BackFillQueryPluginManager $backFillQueryPluginManager
   *   Query plugin manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, Connection $database, AccountInterface $currentUser, BackFillQueryPluginManager $backFillQueryPluginManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->database = $database;
    $this->currentUser = $currentUser;
    $this->backFillQueryPluginManager = $backFillQueryPluginManager;
  }

  /**
   * Backfills entities.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity to backfill.
   * @param \Drupal\Core\Entity\ContentEntityInterface[] $existing
   *   Existing entity references.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   Field definition.
   * @param int $backfill_count
   *   Backfill count.
   * @param array $vocabulary_weights
   *   Array of weights, keyed by vocabulary ID.
   * @param string $backfill_mode
   *   One of the self::MODE_ constants.
   * @param callable|null $query_callback
   *   Query callback that can interact with the query. Takes an instance of
   *   \Drupal\Core\Database\Query\SelectInterface as the first argument.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   Array of entities.
   */
  public function backfillEntity(ContentEntityInterface $entity, array $existing, FieldDefinitionInterface $field_definition, int $backfill_count, array $vocabulary_weights = [], string $backfill_mode = self::MODE_MATCH_ANY, callable $query_callback = NULL) : array {
    if (!$entity->hasField($field_definition->getName())) {
      return [];
    }
    $target_entity_type_id = $field_definition->getSetting('target_type');
    $values = array_map(function (ContentEntityInterface $entity) {
      return $entity->id();
    }, $existing);
    $entity_storage = $this->entityTypeManager->getStorage($target_entity_type_id);
    if (count($values) >= $backfill_count) {
      return $entity_storage->loadMultiple($values);
    }
    $target_bundles = $field_definition->getSetting('handler_settings')['target_bundles'] ?? [];
    return $this->findMatchingEntitiesByTerms($entity, $target_entity_type_id, $target_bundles, $backfill_count, $vocabulary_weights, $backfill_mode, $values, $query_callback);
  }

  /**
   * Match entities using terms.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity to backfill.
   * @param string $target_entity_type_id
   *   Entity type ID to match.
   * @param string[] $target_bundles
   *   Target bundles to match.
   * @param int $quantity
   *   Entities to match.
   * @param array $vocabulary_weights
   *   Array of weights, keyed by vocabulary ID.
   * @param string $backfill_mode
   *   One of the self::MODE_ constants.
   * @param array $existing
   *   Array of entity IDs already determined by other means, these will be
   *   loaded and included in the return.
   * @param callable|null $query_callback
   *   Query callback that can interact with the query. Takes an instance of
   *   \Drupal\Core\Database\Query\SelectInterface as the first argument.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   Array of entities matched along with the original entities passed via
   *   $existing argument.
   */
  public function findMatchingEntitiesByTerms(ContentEntityInterface $entity, string $target_entity_type_id, array $target_bundles, int $quantity, array $vocabulary_weights = [], string $backfill_mode = self::MODE_MATCH_ANY, array $existing = [], callable $query_callback = NULL) : array {
    $entity_storage = $this->entityTypeManager->getStorage($target_entity_type_id);
    $query = $this->database->select('taxonomy_entity_index', 'tei')->fields('tei', ['tid'])
      ->condition('entity_type', $entity->getEntityTypeId())
      ->condition('entity_id', $entity->id());
    if ($revision_id = $entity->getRevisionId()) {
      $query->condition('revision_id', $revision_id);
    }
    $entity_term_ids = $query->execute()->fetchCol();
    if (empty($entity_term_ids)) {
      // Entity has no terms.
      return $existing ? $entity_storage->loadMultiple($existing) : [];
    }
    $term_storage = $this->entityTypeManager->getStorage('taxonomy_term');
    $remaining = $quantity - count($existing);
    if (empty($vocabulary_weights)) {
      $values = array_merge($existing, array_keys($this->queryMatchingEntities($entity, $target_entity_type_id, $entity_term_ids, $target_bundles, $remaining, $existing, $query_callback)));
      return $entity_storage->loadMultiple($values);
    }
    $tid_vid_pairs = $term_storage->getAggregateQuery()
      ->condition('tid', $entity_term_ids, 'IN')
      ->groupBy('vid')
      ->groupBy('tid')
      ->execute();
    asort($vocabulary_weights);
    $values = $existing;
    if (in_array($backfill_mode, [self::MODE_MATCH_ALL, self::MODE_MATCH_ALL_WITH_FALLBACK], TRUE)) {
      $vocabulary_term_ids = array_reduce(array_keys($vocabulary_weights), function (array $carry, $vid) use ($tid_vid_pairs) {
        $carry[$vid] = array_map(function (array $item) {
          return $item['tid'];
        }, array_filter($tid_vid_pairs, function (array $item) use ($vid) {
          return $item['vid'] === $vid;
        }));
        return $carry;
      }, []);
      if (!empty($vocabulary_term_ids)) {
        $first = array_shift($vocabulary_term_ids);
        $all = $this->queryMatchingEntities($entity, $target_entity_type_id, $first, $target_bundles, 0, $values, $query_callback);
        foreach ($vocabulary_term_ids as $set) {
          $set_matches = $this->queryMatchingEntities($entity, $target_entity_type_id, $set, $target_bundles, 0, $values, $query_callback);
          $all = array_reduce(array_keys($set_matches), function (array $carry, $entity_id) use ($set_matches) {
            // This new entity doesn't exist in the existing set, so isn't
            // valid - ignore it.
            if (!array_key_exists($entity_id, $carry)) {
              return $carry;
            }
            $carry[$entity_id] += $set_matches[$entity_id];
            return $carry;
          },
            // Filter out existing values that don't exist in the new matches.
            array_intersect_key($all, $set_matches));
        }
        // Sort by number of matches.
        ksort($all);
        // We passed $length 0 above to avoid unfairly excluding items that may
        // match all, so we need to use array_slice here to cut down the final
        // list to our desired length.
        $values = array_merge($values, array_slice(array_keys($all), 0, $remaining));
        if ($backfill_mode === self::MODE_MATCH_ALL) {
          return $entity_storage->loadMultiple($values);
        }
      }
    }
    $remaining = $quantity - count($values);
    while ($vocabulary_weights && $remaining) {
      // The vocabulary IDs are the keys.
      reset($vocabulary_weights);
      $vocabulary = key($vocabulary_weights);
      array_shift($vocabulary_weights);
      $vocabulary_term_ids = array_map(function (array $item) {
        return $item['tid'];
      }, array_filter($tid_vid_pairs, function (array $item) use ($vocabulary) {
        return $item['vid'] === $vocabulary;
      }));
      if (empty($vocabulary_term_ids)) {
        continue;
      }
      $values = array_merge($values, array_keys($this->queryMatchingEntities($entity, $target_entity_type_id, $vocabulary_term_ids, $target_bundles, $remaining, $values, $query_callback)));
      $remaining = $quantity - count($values);
    }
    return $entity_storage->loadMultiple($values);
  }

  /**
   * Query for matching entities.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Host entity being back-filled.
   * @param string $target_entity_type_id
   *   Type of entity being reference.
   * @param array[] $common_term_ids
   *   Sets of term IDs to match on. Multiple sets can be passed where multiple
   *   matches are required.
   * @param array $target_bundles
   *   Target bundles to match on.
   * @param int $length
   *   Number of matches required.
   * @param array $exclude
   *   Array of entities to exclude.
   * @param callable $query_callback
   *   Query callback that can interact with the query. Takes an instance of
   *   \Drupal\Core\Database\Query\SelectInterface as the first argument.
   *
   * @return array
   *   Array of matches keyed by entity ID with the values being the number of
   *   terms in common.
   */
  private function queryMatchingEntities(ContentEntityInterface $entity, string $target_entity_type_id, array $common_term_ids, array $target_bundles, int $length, array $exclude, callable $query_callback = NULL) : array {
    if (empty($common_term_ids)) {
      return [];
    }
    if ($entity->getEntityTypeId() === $target_entity_type_id) {
      $exclude[] = $entity->id();
    }
    $query = $this->database->select('taxonomy_entity_index', 'tei')
      ->fields('tei', ['entity_id'])
      ->condition('entity_type', $target_entity_type_id)
      ->condition('tid', $common_term_ids, 'IN')
      ->groupBy('entity_id');
    if ($exclude) {
      $query->condition('entity_id', $exclude, 'NOT IN');
    }
    $alias = $query->addExpression('COUNT(entity_id)', 'matches');
    // Order by most matches, then tid and entity id (for consistent results).
    $query->orderBy($alias, 'DESC')
      ->orderBy('tid', 'ASC')
      ->orderBy('entity_id', 'DESC');
    if ($target_bundles) {
      $query->condition('bundle', $target_bundles, 'IN');
    }
    if ($query_callback) {
      $query_callback($query);
    }
    if ($length) {
      // When matching on ALL, we don't want to exclude items until we've got
      // all the matches for each vocabulary.
      $query->range(0, $length);
    }
    try {
      /** @var \Drupal\backfill_formatter\BackFillQueryPluginInterface $alter */
      $alter = $this->backFillQueryPluginManager->createInstance('default:' . $target_entity_type_id);
      $alter->alterQuery($this->entityTypeManager, $query, $this->currentUser, $entity->language()->getId());
    }
    catch (PluginException $e) {
      // Continue.
    }
    return array_map(function ($row) {
      return $row->matches;
    }, $query->execute()->fetchAllAssoc('entity_id'));
  }

}
